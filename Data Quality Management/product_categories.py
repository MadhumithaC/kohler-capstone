import pandas as pd
import constant as c

product_categories=pd.read_excel(c.product_categories_location,engine = "openpyxl")
checker = {}

brand_catergory_error = -1
level1_product_code_datatype_error = -1
null_threshold_flag = -1
required_column_check_flag = -1


def datatype_check():
    '''Sets datatype error flag to 1 if product code datatype is not int'''
    try:
        product_categories[c.level1_prod_code]=(product_categories[c.level1_prod_code]).dropna().astype(int)
        level1_product_code_datatype_error = 0
    except:
        level1_product_code_datatype_error = 1
    return level1_product_code_datatype_error

def brand_check():
    '''Checks for brand categories '''

    for i in product_categories[c.pc_brand].dropna().unique():
        if i in c.brand_unique_list:
            brand_catergory_error = 0
        else:
            brand_catergory_error = 1

    return brand_catergory_error

def brand_name_check():
    '''Checks if brand_name first character is alphanumeric'''
    for i in product_categories[c.brand_name]:
        if i[0].isalnum():
            brand_name_error = 0
        else:
            brand_name_error = 1

        return brand_name_error

def null_check():
    ''' checks for null percentage of required columns and updates threshold flag'''
    print(required_column_check_flag)
    mpnull = product_categories[c.pc_required_columns].isnull().sum()/len(product_categories[c.pc_required_columns])*100
    totalmiss = mpnull.sum().round(2)
    if totalmiss <= 30:
        null_threshold_flag = 0    
    else:
        null_threshold_flag = 1
    return null_threshold_flag

def column_checks():
    '''checks for existence of required columns '''
    if set(c.pc_required_columns).issubset(product_categories.columns):
        required_column_check_flag = 0
    else:
        required_column_check_flag = 1

    return required_column_check_flag

def run_checks():
    ''' runs all checks and returns a dictionary with checker name and flag 
        ex: null_threshold_check : 1 if file does not pass null threshold check '''
    checker[c.pc_err1] = brand_check()
    checker[c.pc_err2] = datatype_check()
    checker[c.pc_err3] = brand_name_check()
    checker[c.pc_err4] = null_check()
    checker[c.pc_err5] = column_checks()
    print(checker)
    return checker 
run_checks()
