import pandas as pd
import constant as c 

material_product =pd.read_excel(c.material_product_location,engine = "openpyxl")

checker = {}
product_code_datatype_error = -1
material_error = -1
null_threshold_flag = -1
required_column_check_flag = -1


def datatype_check():
    '''
    checks for different datatype errors in columns 
    '''
    try:
        material_product[c.product_code]=(material_product[c.product_code]).astype(int)
        c.product_code_datatype_error = 0
    except:
        c.product_code_datatype_error = 1
    return c.product_code_datatype_error

def brand_check():
    '''checks for categories in brand'''
    for i in material_product[c.brand]:
        if i in c.brand_unique_list:
            c.brand_catergory_error = 0
        else:
            c.brand_catergory_error = 1

    return c.brand_catergory_error

def material_check():
    '''checks for starting character of material being alpha numeric'''
    for i in material_product[c.material]:
        if i[0].isalnum():
            c.material_error = 0
        else:
            c.material_error = 1

        return c.material_error

def null_check():
    ''' checks for null percentage and updates threshold flag'''
    mpnull = material_product[c.mp_required_columns].isnull().sum()/len(material_product[c.mp_required_columns])*100
    totalmiss = mpnull.sum().round(2)
    if totalmiss <= 30:
        c.null_threshold_flag = 0    
    else:
        c.null_threshold_flag = 1
    return c.null_threshold_flag

def column_checks( ):
    '''checks for existence of required columns '''
    if set(c.mp_required_columns).issubset(material_product.columns):
        required_column_check_flag = 0
    else:
        required_column_check_flag = 1

    return required_column_check_flag

def run_checks():
    ''' runs all checks and returns a dictionary with checker name and flag 
        ex: null_threshold_check : 1 if file does not pass null threshold check '''
    checker[c.mp_error1] = brand_check()
    checker[c.mp_error2] = datatype_check()
    checker[c.mp_error3] = material_check()
    checker[c.mp_error4] = null_check()
    checker[c.mp_error5] = column_checks()
    print(checker)
    return checker

run_checks()