import pandas as pd 
#from pprint import pprint
import material_product as mp
import material_description as md
import day_level as dl
import product_categories as pc
import constant as c 

from itertools import chain


import smtplib, ssl

port = 465  # For SSL
password = 'dev@kohler99' #sender email password
server = "smtp.gmail.com"
sender_mail = 'dev.kohlerr@gmail.com'
receiver_mail = 'ruthvika.mohan@gmail.com'

#port = 587  # For SSL
#password = 'dev@kohler99' #sender email password
#server = "smtp-mail.outlook.com"
#sender_pass = 'Mathco@123#'
#sender_mail = 'noreply.kohler@themathcompany.com'
#receiver_mail = 'ruthvika.mohan@themathcompany.com'


# Create a secure SSL context
context = ssl.create_default_context()


# Run all checks

print("\n\nMaterial Product\n\n")
mp_dict = mp.run_checks()
print("\n\nMaterial Description\n\n")
md_dict = md.run_checks()
print("\n\nProduct categories\n\n")
pc_dict = pc.run_checks()
print("\n\nDay Level\n\n")
dl_dict = dl.run_checks()

## Typing out final error mail 
def error_message():
    message = c.line1

    for key,value in mp_dict.items():
        if value == 1:
            message.append(str(key) + "\n")
    message.append(c.breaker)

    message.append(c.line2)

    for key,value in md_dict.items():
        if value == 1:
            message.append(str(key) + "\n")
    message.append(c.breaker)

    message.append(c.line3)

    for key,value in pc_dict.items():
        if value == 1:
            message.append(str(key) + "\n")
    message.append(c.breaker)
            
    message.append(c.line4)

    for key,value in dl_dict.items():
        if value == 1:
            message.append(str(key) + "\n")
    message.append(c.breaker)

    message.append(c.ending)   

    message = "".join(message)
    print(message)
    return message

## checks for error and then call error message function
dict_list = [mp_dict.values(),md_dict.values(),pc_dict.values(),dl_dict.values()]
if 1 in chain(dict_list):
    print("error")
    message = error_message()
    
    with smtplib.SMTP_SSL(server, port, context=context) as server:
        server.login(sender_mail, password)
        server.sendmail(sender_mail, receiver_mail, message)
    
else:
    with smtplib.SMTP_SSL(server, port, context=context) as server:
        print("\n\nNo errors. Can be processed\n\n")
        server.login(sender_mail, password)
        server.sendmail(sender_mail, receiver_mail, "No errors")

       



